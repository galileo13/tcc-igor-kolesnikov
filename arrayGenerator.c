#include <stdio.h>
#include <time.h>
#include <stdlib.h>

// função que gera os arrays de numeros alereatorios de tamnaho arbitrario
// e les escreve para os arquivos respectivos 
void gerador(double *Array, char *nomeArquivo, int ITERATIONS)
{
	// definição de variaavel de acesso para arquivo
	FILE *fp;
	
	// gerando array
	for (int i = 0; i < ITERATIONS; ++i)
	{
		Array[i] = (rand() % 100);
	}

	// escrevendo array para arquivo
	fp = fopen(nomeArquivo, "w+");
	for (int i = 0; i < ITERATIONS; ++i)
	{
		fprintf(fp, "%lf ", Array[i]);
	}
	// fechando conexao com arquivo
	fclose(fp);
}

int main() 
{
	// recebendo o tamanho do array
	int ITERATIONS = 0;
	scanf("%d", &ITERATIONS);

	// definindo o seed para geração dos numeros alereatorios
	srand(time(NULL));

	double *A, *B, *C; 
	// allocando memoria para os arrays
	A = malloc(ITERATIONS * sizeof(double));
	B = malloc(ITERATIONS * sizeof(double));
	C = malloc(ITERATIONS * sizeof(double));

	// gerando os arrays
	char nomeA[] = "A.txt";
	char nomeB[] = "B.txt";
	gerador(A, nomeA, ITERATIONS);
	gerador(B, nomeB, ITERATIONS);

	printf("%d\n", ITERATIONS);
}