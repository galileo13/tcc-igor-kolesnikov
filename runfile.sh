#!/bin/bash

# Exemplo de uso:
# ./runfile.sh NUMERO
# onde NUMERO sera a quantidade de iteraçoes desejada (deve ser >=11)

#animação de contagem
animacao()
{
    sleep 1 
    echo -n .
    sleep 1
    echo -n .
    sleep 1
    echo -n .
    sleep 1
    echo -n .
    sleep 1
    echo -n .
}

N=$1

#1) 
echo Gerando array com $N caracters alereatorios

#animação de contagem
#animacao

gcc -o arrayGenerator arrayGenerator.c
echo $N | ./arrayGenerator >> nElementos.txt
echo Pronto!
echo

#2) 
echo Executando o benchmark no CPU
#animação de contagem
#animacao

gcc -o cpu cpu.c -lm
START=$(date +%s.%N)
echo $N | ./cpu >> cpuTime.txt
echo Pronto!
echo

#3) 
echo Executando o benchmark no CPU com OpenMP
#animação de contagem
#animacao

gcc -fopenmp -o cpu_omp cpu_omp.c -lm
echo $N | ./cpu_omp >> cpu_ompTime.txt
echo Pronto!
echo

#4) 
echo Executando o benchmark no GPU
#animação de contagem
#animacao

nvcc -Wno-deprecated-gpu-targets -o gpu gpu.cu -lm
echo $N | ./gpu >> gpuTime.txt
echo Pronto!
echo

#5) 
echo Comparando os resultados
#animação de contagem
#animacao

gcc -o comparandoResultados comparandoResultados.c
echo Pronto!
echo

echo $N | ./comparandoResultados