#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

// instanciando o temporizador
clock_t start, end;
double cpu_time_used;

// função que sobrecarrega o CPU
void cpuBench(double *A, double *B, double *C, int ITERATIONS, int Y_ITERATIONS)
{
    for (int i = 0; i < ITERATIONS; i++)
    {
        // para cada elemento de i
        A[i] = A[i] * 1.123f * sin(i / 1.111f); // bench
        B[i] = B[i] * 1.123f * sin(i / 1.111f); // bench

        for (int y = 0; y < ITERATIONS - Y_ITERATIONS; y++)
        {
            // tem que usar um elemento de y
            //C[i] = A[i] + B[i]; // debug
            // double root = sqrtf(y);
            C[i] = (A[i] + B[i]) * sin(y / 0.111f) - 0.25f; // bench
        }
    }
}

// funçaõ que le os arrays com nomes respectivos
void lerArray(double *Array, char *nomeArquivo, int ITERATIONS)
{
    FILE *fp;
    fp = fopen(nomeArquivo, "r");
    for (int i = 0; i < ITERATIONS; ++i)
    {
        fscanf(fp, "%lf ", &Array[i]);
    }
    fclose(fp);
}

// salvando resultados para o arquivo
void salvarArray(double *C, int ITERATIONS)
{
    FILE *fp;
    fp = fopen("resultadosCPU.txt", "w+");
    for (int i = 0; i < ITERATIONS; ++i)
    {
        fprintf(fp, "%.3f \n", C[i]);
    }
    fclose(fp);
}

int main()
{
    // recebendo o tamanho do array
    int ITERATIONS, Y_ITERATIONS = 10;
    scanf("%d", &ITERATIONS);

    // definição dos variaveis como ponteiros para nao ter limitação na memoria de stack
    double *A, *B, *C;
    // alocação de memoria para as variaveis
    A = malloc(ITERATIONS * sizeof(double));
    B = malloc(ITERATIONS * sizeof(double));
    C = malloc(ITERATIONS * sizeof(double));

    char nomeA[] = "A.txt";
    char nomeB[] = "B.txt"; 
    // lendo os arrays
    lerArray(A, nomeA, ITERATIONS);
    lerArray(B, nomeB, ITERATIONS);

    // executando a função de benchmark e medindo seu tempo de execução
    clock_t t;
    t = clock();
    cpuBench(A, B, C, ITERATIONS, Y_ITERATIONS);
    t = clock() - t;
    double cpuTime = ((double)t) / CLOCKS_PER_SEC; // in seconds

    // salvando resultados
    salvarArray(C, ITERATIONS);

    printf("%lf\n", cpuTime);

    free(A);
    free(B);
    free(C);

    return 0;
}