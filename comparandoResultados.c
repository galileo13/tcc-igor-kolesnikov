#include <stdio.h>
#include <time.h>
#include <stdlib.h>

// recebendo os resultados e tempo dos arquivos que foram gerados nas etapas anteriores
double lendoResultados(double *results, char *nomeArquivoReultados, char *nomeArquivoTempo, int ITERATIONS)
{
    FILE *fp;
    fp = fopen(nomeArquivoReultados, "r");
    for (int i = 0; i < ITERATIONS; ++i)
    {
        fscanf(fp, "%lf ", &results[i]);
    }
    fclose(fp);
    
    // lendo tempo de execução
    double time = 0;
    fp = fopen(nomeArquivoTempo, "r");
    fscanf(fp, "%lf ", &time);
    fclose(fp);
    return time;
}

int main()
{
    // recebendo o tamanho do array
    int ITERATIONS = 0;
    scanf("%d", &ITERATIONS);

    double *GPU, *CPU, *CPU_OMP;
    GPU     = malloc(ITERATIONS * sizeof(double));
    CPU     = malloc(ITERATIONS * sizeof(double));
    CPU_OMP = malloc(ITERATIONS * sizeof(double));

    // lendo array dos resultados de GPU
    double cpuTime     = lendoResultados(CPU, "resultadosCPU.txt", "cpuTime.txt", ITERATIONS);
    // lendo array dos resultados de CPU
    double gpuTime     = lendoResultados(GPU, "resultadosGPU.txt", "gpuTime.txt", ITERATIONS);
    // lendo array dos resultados de CPU
    double cpu_ompTime = lendoResultados(CPU_OMP, "resultadosCPU_OMP.txt", "cpu_ompTime.txt", ITERATIONS);

    // gerando 10 numeros aleatorios para comparação dos valores de GPU e CPU
    srand(time(NULL));
    int comparadores[10];
    for (int i = 0; i < 10; i++)
    {
        comparadores[i] = rand() % ITERATIONS;
    }

    int tudo_certo = 1;
    for (int i = 0; i < 10; i++)
    {
        if (GPU[comparadores[i]] != CPU[comparadores[i]] || GPU[comparadores[i]] != CPU_OMP[comparadores[i]])
        {
            tudo_certo = 0;
        }
    }





    int nThreads = 12;
    double ranking[3];

    if (tudo_certo)
    {
        printf("Todos valores coincidem!\n");
        printf("Benchmark foi executado com exito!\n");
        printf("Tempo de execução de CPU com single thread => %lf\n", cpuTime);
        printf("Tempo de execução de CPU com OpenMP com 12 threads => %lf\n", cpu_ompTime);
        printf("Tempo de execução de GPU => %lf\n", gpuTime);
        if(cpuTime > cpu_ompTime && cpu_ompTime > gpuTime)
        {
            ranking[0] = gpuTime;
            ranking[1] = cpu_ompTime;
            ranking[2] = cpuTime;
            printf("O GPU foi mais rapida do que o CPU em %lf vezes e mais rapida do que CPU com %d thread em %lf.\n", cpuTime/gpuTime, nThreads, cpu_ompTime/gpuTime);
        }
        else if (cpuTime > cpu_ompTime && cpu_ompTime < gpuTime)
        {
            ranking[0] = cpu_ompTime;
            ranking[1] = cpuTime;
            ranking[2] = gpuTime;
        }
        else if (cpuTime < cpu_ompTime && gpuTime > cpuTime)
        {
            ranking[0] = cpuTime;
            ranking[1] = gpuTime;
            ranking[2] = cpu_ompTime;
            printf("O CPU foi mais rapido do que a GPU em %lf vezes e mais rapido do que CPU com %d threads em %lf.\n", gpuTime/cpuTime, nThreads, cpu_ompTime/cpuTime);
        }
        else if (cpuTime < cpu_ompTime && gpuTime < cpuTime)
        {
            ranking[0] = gpuTime;
            ranking[1] = cpuTime;
            ranking[2] = cpu_ompTime;
            printf("O GPU foi mais rapida do que o CPU em %lf vezes e mais rapida do que CPU com %d thread em %lf.\n", cpuTime/gpuTime, nThreads, cpu_ompTime/gpuTime);
        }
        
    
        printf("ranking[0] = %lf\n", ranking[0]);
        printf("ranking[1] = %lf\n", ranking[1]);
        printf("ranking[2] = %lf\n", ranking[2]);
    }
    else
    {
        printf("Os valores nao coincidem\n");
        printf("Benchmark nao foi executado com sucesso\n");
    }
}