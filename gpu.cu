#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

// instanciando o temporizador
clock_t start, end;
double cpu_time_used;

// funçao que sera executada no GPU
__global__ void bench(double *A, double *B, double *C, int ITERATIONS, int Y_ITERATIONS)
{
    // defenindo as variaveis auxiliares, onde:
    // stride - é os pulos que programa vai fazer para passar de um block de execuçao para outro
    // obs: mas isso somente acontece quando nao ha sufuciente threads para execução das todas tarefas de uma vez
    // obs 2: de grosso modo, quase sempre

    // index  - é indicador de execução de cada thread dentro de um block
    int index, stride;

    // calculo das variaveis auxiliares usando as variaveis embutidas de CUDA API
    // onde:
    // blockIdx.x - o indicador de bloco que esta executando
    // blockDim.x -  o tamanho de block em execução (quantidade dos thread que contem)
    // threadIdx.x - o indicador de threadIdx.x em execução

    index = blockIdx.x * blockDim.x + threadIdx.x;
    // gridDim - a quantidade dos blocks detro de uma grid (a estrutura de nivel superior de execução de um kernel[programa em uma GPU])
    stride = blockDim.x * gridDim.x;

    // POR EXEMPLO:
    // nos vamos executar um kernel com tres blocks, onde cada um ira conter cinco threads
    // nesse caso teremos:
    // index (0) = blockIdx.x (0) * blockDim.x(5) + threadIdx.x(0) na 1 execução
    // index (1) = blockIdx.x (0) * blockDim.x(5) + threadIdx.x(1) na 2 execução
    // index (2) = blockIdx.x (0) * blockDim.x(5) + threadIdx.x(2) na 3 execução
    // index (3) = blockIdx.x (0) * blockDim.x(5) + threadIdx.x(3) na 4 execução
    // index (4) = blockIdx.x (0) * blockDim.x(5) + threadIdx.x(4) na 5 execução
    // index (5) = blockIdx.x (1) * blockDim.x(5) + threadIdx.x(0) na 6 execução
    // index (6) = blockIdx.x (1) * blockDim.x(5) + threadIdx.x(1) na 7 execução
    // ...
    // index (14) = blockIdx.x (2) * blockDim.x(5) + threadIdx.x(4) na ultima execução

    // enquanto o stride iria defenir a quantidade dos threads executados simultaneamente
    // stride (15) = blockDim.x (5) * gridDim.x (3) - na 1 execução -> SOMENTE UMA EXECUÇÂO DE LOOP
    // isso acontece para automatizar alocação e dealocação dos blocos de execução
    // fora de questao que isso pode traser beneficios significantes nos kernels simples (descrever o que é kernel)
    // basicamente isso é unico intuito dessa forma de execução para caso dos ararys unidimensionais(1D)

    // os blocos que serao formados serao distribuidos entre os SM (streaming multiprocessor) que sao disponiveis numa/varias GPUs do sistema
    // para vereficar, deve ser consultada a especificação da placa de video
    // no exemplo da GTX 1060 3GB -> /mnt/Data/MEGA/Science/Iniciacao 3(CUDA)/TCC_Igor/GPU/gtx1060_3gb.txt
    // tendo 9 SMs, cada um podendo acomodar ate 32 blocos
    // warp - quantidade constante dos threads carregados literalment ao mesmo tempo.
    // (calculo inexato, pois processamento nao acontece da forma sequencial e depende dos warps; cada SM pode receber ate 64 warps dos block cada um contendo 32 threads)
    // fazendo multiplicação obtemom 288 blocks, cada um podendo acomodar ate 1024 thread (respeitando limitação de 2048 thread por SM)
    // OBS: o objetivo principal de um kernel bem escrito é usar massima quantidade dos blocos que possivel para realizar a operação requerida
    // e nao minima quantidade necessaria para execução.

    // loop que executa as operaçoes matematicas
    // for (int i = index; i < ITERATIONS; i =+ stride)
    // {
    //     printf("i = %d; index = %d; stride = %d; blockIdx.x = %d; threadIdx.x = %d - %lf\n",i, index, stride, blockIdx.x, threadIdx.x, C[i] = A[i] + B[i]);
    // }

    for (int i = index; i < ITERATIONS; i = +stride)
    {
        // para cada elemento de i
        A[i] = A[i] * 1.123f * sin(i / 1.111f); // bench
        B[i] = B[i] * 1.123f * sin(i / 1.111f); // bench

        for (int y = 0; y < ITERATIONS - Y_ITERATIONS; y++)
        {
            // tem que usar um elemento de y
            //C[i] = A[i] + B[i]; // debug

            C[i] = (A[i] + B[i]) * sin(y / 0.111f) - 0.25f; // bench
        }
    }
}

// funçaõ que le os arrays com nomes respectivo
void lerArray(double *Array, char *nomeArquivo, int ITERATIONS)
{
    FILE *fp;
    fp = fopen(nomeArquivo, "r");
    for (int i = 0; i < ITERATIONS; ++i)
    {
        fscanf(fp, "%lf ", &Array[i]);
    }
    fclose(fp);
}

// salvando resultados para o arquivo
void salvarArray(double *C, int ITERATIONS)
{
    FILE *fp;
    fp = fopen("resultadosGPU.txt", "w+");
    for (int i = 0; i < ITERATIONS; ++i)
    {
        fprintf(fp, "%.3f \n", C[i]);
    }
    fclose(fp);
}

int main()
{
    // recebendo o tamanho do array
    int ITERATIONS, Y_ITERATIONS = 10;
    scanf("%d", &ITERATIONS);

    // definição dos variaveis como ponteiros para nao ter limitação na memoria de stack
    double *A, *B, *C;

    // alocação de memoria para as variaveis no heap
    cudaMallocManaged(&A, ITERATIONS * sizeof(double));
    cudaMallocManaged(&B, ITERATIONS * sizeof(double));
    cudaMallocManaged(&C, ITERATIONS * sizeof(double));

    // definção dos parametros de execução de kernel
    // onde o blockSize é quantidade dos threads dentro de um block (128-512 considerado como um intervalo perfeito dependendo da operação)
    int blockSize = 256; // como determinar o blockSize perfeito para um programa
    // e o numBlocks é calculado a partir de numero de iteraçoes necessaria e blockSize e corresponde o numero maximo dos blocos
    // que serao alocados para o processamento
    int numBlocks = (ITERATIONS + blockSize - 1) / blockSize;

    char nomeA[] = "A.txt";
    char nomeB[] = "B.txt"; 
    // lendo array A
    lerArray(A, nomeA, ITERATIONS);
    // lendo array B
    lerArray(B, nomeB, ITERATIONS);

    // inicio temporizador
    clock_t t;
    t = clock();
    // execução de kernel
    bench<<<numBlocks, blockSize>>>(A, B, C, ITERATIONS, Y_ITERATIONS);
    // forcar o CPU esperar o GPU terminar a execuçao de kernel para evitar a perda de dados
    cudaDeviceSynchronize();
    // finalização de temporizador
    t = clock() - t;

    // calculo de tempo decorrido em segundos
    double gpuTime = ((double)t) / CLOCKS_PER_SEC;

    // salvando resultados
    salvarArray(C, ITERATIONS);

    printf("%lf\n", gpuTime);

    // liberação de memoria apos termino de execução
    cudaFree(A);
    cudaFree(B);
    cudaFree(C);

    return 0;
}